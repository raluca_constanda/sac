$( document ).ready(function() {
    // populate table with the latest news

    var numberOfNews = 10;
    var data = {"numberOfNews": numberOfNews};

    // creates a request to News.php
    $.ajax({
        dataType: "json",
        type: "POST",
        url: 'Backend/News.php',
        data: data,
        success: function(response) {

            var trHTML = '';
            $.each(response, function (i, item) {
                trHTML += '<tr onclick="showNews(this)"><td>' + item.title + '</td>' +
                    '<td hidden="true">' + item.link + '</td>' +
                    '</tr>';
            });
            $('#news-table').append(trHTML);

        },
        failure: function(err) {
            console.log("Error:" + err);
        }
    });

    $.ajax({
        dataType: "json",
        type: "POST",
        url: 'Backend/index.php',

        success: function(response) {
            console.log("Success:" + response);

            var trHTML = '';
            $.each(response, function (i, item) {
                trHTML += '<tr onclick="showNews(this)"><td>' + item.title + '</td>' +
                    '<td hidden="true">' + item.link + '</td>' +
                    '</tr>';
            });
            $('#news-table1').append(trHTML);

        },

        failure: function(err) {
            console.log("Error:" + err);
        }
    });

});

function showNews(obj){

    var tdArray = obj.getElementsByTagName("td")
    var tdWithLink = jQuery.makeArray(tdArray)[1]
    var link = tdWithLink.textContent

    var data = {"link": link};

    $.ajax({
        dataType: "json",
        type: "POST",
        url: 'Backend/NewsContent.php',
        data: data,
        success: function(response) {

            $('#news-title').html(response['title']);
            $('#news-text').html(response['content']);

        },
        failure: function(err) {
            console.log("Error:" + err);
        }
    });
}