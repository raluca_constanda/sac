<?php
/**
 * Created by PhpStorm.
 * User: raluca
 * Date: 1/10/16
 * Time: 5:51 PM
 */
require_once '../API/alchemyapi_php/alchemyapi.php';

// get link from front-end
$link = $_POST['link'];

// creates an alchemy object
$alchemyAPI = new AlchemyAPI();

$image = $alchemyAPI->image_keywords('url', $link, array('extractMode'=>'trust-metadata'));

// get news content (text)
$text = $alchemyAPI->text('url', $link, null);

if ($text['status'] == 'OK') {

//    echo 'text: ',PHP_EOL, $text['text'], PHP_EOL;
} else {
    echo 'Error in the text extraction call: ', $text['statusInfo'];
}

// get news title
$title = $alchemyAPI->title('url',$link, null);

if ($title['status'] == 'OK') {
//    echo print_r($title);

//    echo 'title: ', $title['title'], PHP_EOL;
} else {
    echo 'Error in the title extraction call: ', $title['statusInfo'];
}

$response['title'] = $title['title'];
$response['content'] = $text['text'];

echo json_encode($response);