<?php
require '../API/PHP-Stanford-NLP/autoload.php';
require '../API/PHP-Stanford-NLP/PostTaggerParser.php';


class Bookmark {

    public $_bookmarks = [];
    public $_words = [];

    public function __construct() {

        $handle = fopen("bookmarks.html", "r");
        if ($handle) {

            while (($line = fgets($handle)) !== false) {
                // process the line read
                // delete white spaces from the begining of the line
                $line = ltrim($line);
                // get only the titles of the bookmarks
                if ($this->startsWith($line, "<DT>") && $this->startsWith($line, "<DT><A HREF=")) {
                    $line = str_replace("</A>", "", $line);
                    var_dump($line);
                    // get the content of the title
                    $x = strrpos($line, ">");
                    $line = substr($line, $x + 1);
                    // remove white spaces from the begining of the title
                    $line = ltrim($line);
                    // add to the bookmarks array
                    if (!empty($line))
                        array_push($this->_bookmarks, $line);
                }
            }
            fclose($handle);
        }
    }

    public function insertBookmarksInDb(){

        $parser = new \StanfordNLP\PostTaggerParser();
        for($i = 0; $i < count($this->_bookmarks); $i++) {
            $parser->setSentence($this->_bookmarks[$i]);

            $parserResult = $parser->getWords();
            var_dump($parserResult);
            if(count($parserResult) > 0) {

                $this->_words = array_merge($this->_words, $parserResult);
            }
        }

        $connection = new \MongoClient();
        $connection->ARR->Bookmarks->insert(['words'=> $this->_words]);
    }

    function startsWith($haystack, $needle) {

        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
}
