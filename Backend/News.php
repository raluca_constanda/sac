<?php
/**
 * Created by PhpStorm.
 * User: raluca
 * Date: 1/10/16
 * Time: 2:29 PM
 */

//var_dump($_GET);
$newsLimit = $_POST['numberOfNews'];

$connection = new MongoClient();
$cursor = $connection->ARR->News->find(array('topic' => 'News'))->sort(["pubDate" => -1])->limit(10);

$documents = [];

foreach ($cursor as $entity) {
    $doc = [];
    $doc['title'] = $entity['title'];
    $doc['link'] = $entity['link'];
    $doc['date'] = $entity['pubDate'];
//    var_dump($doc);
    array_push($documents, $doc);
}

//var_dump($documents);
echo json_encode($documents);