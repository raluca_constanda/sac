<?php

require 'Bookmarks.php';

//$bookmarks = new Bookmark();
//var_dump($bookmarks->_bookmarks);
//$bookmarks->insertBookmarksInDb();

$connection = new \MongoClient();
$words = $connection->ARR->Bookmarks->findOne();

$news = $connection->ARR->News->find()->limit(10);

$words = $words['words'];
$documents = [];

for($i = 0; $i < count($words); $i++) {
    if(strlen($words[$i]) > 1) {

        foreach($news as $entity) {

            $newsTitle = $entity['title'];

            if (strpos($newsTitle,$words[$i]) !== false) {

                $doc = [];
                $doc['title'] = $entity['title'];
                $doc['link'] = $entity['link'];
                $doc['date'] = $entity['pubDate'];
                array_push($documents, $doc);
            }
        }
    }
}
echo json_encode($documents);