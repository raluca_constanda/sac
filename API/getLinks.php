<?php 

	$urls = [
		'News' => 'http://www.npr.org/rss/rss.php?id=1001',
		'Arts' => 'http://www.npr.org/rss/rss.php?id=1008',
		'Health' => 'http://www.npr.org/rss/rss.php?id=1007',
		'Politics' => 'http://www.npr.org/rss/rss.php?id=1014',
		'Sports' => 'http://www.npr.org/rss/rss.php?id=1055',
		'Tech' => 'http://www.npr.org/rss/rss.php?id=1019'
	];
	
	$connection = new MongoClient();

	foreach ($urls as $topic => $url) {

		$ch = curl_init();
		$params = array(
			// CURLOPT_HEADER => TRUE,
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => 1
		);

		// make php curl request
		curl_setopt_array($ch, $params);
		
		// xml format
		$result = curl_exec($ch);
		if($result === false) {
			echo "Error curl". curl_error($ch);
			return;
		}

		$xml = simplexml_load_string($result);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		$items = $array["channel"]['item'];

		for ($i=0; $i < count($items); $i++) {

			$entity = [];	
			// create db entity 
			$entity['title'] 	= $items[$i]['title'];
			$entity['pubDate'] 	= $items[$i]['pubDate'];
			$entity['link'] 	= $items[$i]['link'];
			$entity['topic'] 	= $topic; 

			// insert entty in db
			$connection->ARR->News->insert($entity);
		}
	}