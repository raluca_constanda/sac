<?php

require 'autoload.php';
use StanfordNLP\POSTagger;

$st = new POSTagger('stanford-postagger-2014-10-26/models/english-left3words-distsim.tagger',
    'stanford-postagger-2014-10-26/stanford-postagger.jar');

$result = $st->tag(explode(' ', "What does the fox say?"));

var_dump($result);
//"What_WP does_VBZ the_DT fox_NN say_VB ?_."