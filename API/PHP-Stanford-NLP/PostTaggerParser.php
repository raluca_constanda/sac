<?php

namespace StanfordNLP;
//require 'PostTaggerParser.php';
require 'autoload.php';

class PostTaggerParser {

    public $_sentence = '';
    public $_posTagger = null;

    /**
     * PostTaggerParser constructor.
     */
    public function __construct(){

        $this->_posTagger = new POSTagger(__DIR__ . '/stanford-postagger-2014-10-26/models/english-left3words-distsim.tagger',
            __DIR__.'/stanford-postagger-2014-10-26/stanford-postagger.jar');
    }

    public function setSentence($sentence){

        $this->_sentence = $sentence;
    }

    public function getWords() {

        $nlpResult = $this->_posTagger->tag(explode(' ', $this->_sentence));

        //"What_WP does_VBZ the_DT fox_NN say_VB ?_."
        $words = explode(" ", $nlpResult);
        $result = [];
        for( $i = 0; $i < count($words); $i++) {

            $word = explode("_", $words[$i])[0];
            $wordType = explode("_", $words[$i])[1];

            if($wordType == 'NN') {
                array_push($result, $word);
            }
        }

        return $result;
    }
}